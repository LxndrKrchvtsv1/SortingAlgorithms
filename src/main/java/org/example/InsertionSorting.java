package org.example;

import java.util.Arrays;

import static org.example.Main.initArray;

public class InsertionSorting {
    public static int[] main(int size) {
        int[] array = initArray(size);

        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            for (int j = i - 1; j >= 0; j--) {
                if (array[j] > temp) {
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}
