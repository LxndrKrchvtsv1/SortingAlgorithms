package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] selectionSortedArray = SelectionSorting.main(5);
        int[] bubbleSortedArray = BubbleSorting.main(5);
        int[] insertionSorting = InsertionSorting.main(5);

        displayArray(selectionSortedArray);
        displayArray(bubbleSortedArray);
        displayArray(insertionSorting);
    }

    public static int[] initArray(int size) {
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * size);
        }

        return array;
    }

    public static void displayArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}