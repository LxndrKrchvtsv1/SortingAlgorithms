package org.example;

import static org.example.Main.initArray;

public class BubbleSorting {
    public static int[] main(int size) {
        int[] array = initArray(size);

        for (int i = 1; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}
